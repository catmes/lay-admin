<?php
use Catmes\LayAdmin\Components\Template;
use Catmes\LayAdmin\Components\UploadMulti;

/* @var UploadMulti $uploadMulti; */

$template = Template::getInstance();
$csrfTokenField = $template->getCsrfTokenField();
$csrfToken = $template->getCsrfToken();
$form = $uploadMulti->getForm();

$jsPostData = "var postData={{$csrfTokenField}: \"{$csrfToken}\"};";
?>

<div class="layui-upload">
    <?php
    if($form){
        echo $form->begin();
        foreach ($form->getFields() as $field){
            echo $field;
        }
        echo $form->end();
    }?>

    <div style="max-width: 1000px; display: block">
        <button type="button" class="layui-btn layui-btn-normal" id="select-files-btn">选择多文件</button>
        <button type="button" class="layui-btn" id="<?php echo $uploadMulti->getUploadBtnId(); ?>">开始上传</button>
    </div>


    <div class="layui-upload-list" style="max-width: 1000px;">
        <table class="layui-table">
            <colgroup>
                <col>
                <col width="150">
                <col width="260">
                <col width="150">
            </colgroup>
            <thead>
            <tr><th>文件名</th>
                <th>大小</th>
                <th>上传进度</th>
                <th>操作</th>
            </tr></thead>
            <tbody id="demoList"></tbody>
        </table>
    </div>

</div>

<script>
    layui.use(['element','upload','form'], function () {

        var $ = layui.jquery,
            element = layui.element,
            form = layui.form,
            upload = layui.upload;

        var uploading = false;

        form.render();

        <?php echo $jsPostData; ?>

        //演示多文件列表 最多可上传100个
        var uploadListIns = upload.render({
            elem: '#select-files-btn'
            ,elemList: $('#demoList') //列表元素对象
            ,url: '<?php echo $uploadMulti->getUrl(); ?>'
            ,accept: '<?php echo $uploadMulti->getAcceptFileType(); ?>'
            ,multiple: true
            ,number: 100
            ,auto: false
            ,bindAction: '#<?php echo $uploadMulti->getUploadBtnId(); ?>'
            ,data: postData
            ,before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致，可参见上文。

                console.log(uploading)
                if(uploading === true ){
                    layer.msg('文件上传中，请耐心等待');
                    this.url = null;
                    return ;
                }
                this.url = '<?php echo $uploadMulti->getUrl(); ?>'
                uploading = true;

                <?php if($form): ?>
                var formValues = <?php echo $form->getJsValue(); ?>;
                for (let key in formValues){
                    postData[key] = formValues[key]
                }
                this.data = postData;
                <?php endif; ?>

            }
            ,choose: function(obj){
                var that = this;
                var files = this.files = obj.pushFile(); //将每次选择的文件追加到文件队列
                //读取本地文件
                obj.preview(function(index, file, result){
                    var tr = $(['<tr id="upload-'+ index +'">'
                        ,'<td>'+ file.name +'</td>'
                        ,'<td>'+ (file.size/1014).toFixed(1) +'kb</td>'
                        ,'<td><div class="layui-progress" lay-filter="progress-demo-'+ index +'"><div class="layui-progress-bar" lay-percent=""></div></div></td>'
                        ,'<td>'
                        ,'<button class="layui-btn layui-btn-xs demo-reload layui-hide">重传</button>'
                        ,'<button class="layui-btn layui-btn-xs layui-btn-danger demo-delete">删除</button>'
                        ,'</td>'
                        ,'</tr>'].join(''));

                    //单个重传
                    tr.find('.demo-reload').on('click', function(){
                        obj.upload(index, file);
                    });

                    //删除
                    tr.find('.demo-delete').on('click', function(){
                        delete files[index]; //删除对应的文件
                        tr.remove();
                        uploadListIns.config.elem.next()[0].value = ''; //清空 input file 值，以免删除后出现同名文件不可选
                    });

                    that.elemList.append(tr);
                    element.render('progress'); //渲染新加的进度条组件
                });
            }
            ,done: function(res, index, upload){ //成功的回调
                var that = this;
                if(res.code == 0){ //上传成功
                    var tr = that.elemList.find('tr#upload-'+ index)
                        ,tds = tr.children();
                    tds.eq(3).html(''); //清空操作
                    delete this.files[index]; //删除文件队列已经上传成功的文件
                    return;
                }
                layer.msg(res.msg);
                this.error(index, upload);
            }
            ,allDone: function(obj){ //多文件上传完毕后的状态回调
                // console.log(JSON.stringify(obj)) {"total":2,"successful":2,"aborted":0}
                layer.msg('上传成功');
                uploading = false;
            }
            ,error: function(index, upload){ //错误回调
                var that = this;
                var tr = that.elemList.find('tr#upload-'+ index)
                    ,tds = tr.children();
                tds.eq(3).find('.demo-reload').removeClass('layui-hide'); //显示重传
                // console.log(index)
            }
            ,progress: function(n, elem, e, index){
                element.progress('progress-demo-'+ index, n + '%'); //执行进度条。n 即为返回的进度百分比
            }
        });

        $("#<?php echo $uploadMulti->getUploadBtnId()?>").click(function (){

        });

        // uploadListIns.reload({data:postData});
    })
</script>

