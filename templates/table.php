<?php

use Catmes\LayAdmin\Components\Table;

/** @var Table $table */

?>
<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main">

        <?php if($table->getFieldSearches()): ?>
        <fieldset class="table-search-fieldset">
            <legend>搜索信息</legend>
            <div style="margin: 10px 10px 10px 10px">
                <form class="layui-form layui-form-pane" action="">

                    <div class="layui-form-item">
                        <?php
                        foreach ($table->getFieldSearches() as $search){
                            echo $search;
                        }
                        ?>
                        <div class="layui-inline">
                            <button type="submit" class="layui-btn layui-btn-normal"  lay-submit lay-filter="data-search-btn"><i class="layui-icon"></i> 搜 索</button>
                        </div>
                        <div class="layui-inline">
                            <button type="submit" class="layui-btn layui-btn-primary"  lay-submit lay-filter="data-reset-btn">重 置</button>
                        </div>
                    </div>
                </form>

            </div>
        </fieldset>
        <?php endif; ?>

        <script type="text/html" id="toolbarDemo">
            <div class="layui-btn-container">
                <?php foreach ($table->getToolbarButtons() as $button){ echo $button; } ?>
            </div>
        </script>

        <table class="layui-hide" id="<?php echo $table->getTableId(); ?>" lay-filter="<?php echo $table->getLayFilterId(); ?>"></table>

        <?php foreach ($table->getOperates() as $operate): ?>
            <script type="text/html" id="<?php echo $operate->getItemToolbarId() ?>">
                <?php
                foreach ($operate->getButtons() as $button){
                    $ifJs = $button->getIfJs();
                    $ifBegin = '';
                    $ifEnd = '';
                    !empty($ifJs) && $ifBegin = "{{# if ({$ifJs}){ }}";
                    !empty($ifJs) && $ifEnd = "{{# } }}";
                    echo $ifBegin;
                    echo $button;
                    echo $ifEnd;
                }
                ?>
            </script>
        <?php endforeach; ?>


    </div>
</div>

<?php echo $table->getLayJs()->render(); ?>
