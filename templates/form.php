<?php

use Catmes\LayAdmin\Components\Form;

/* @var Form $form */

?>

<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main">

<!--        <blockquote class="layui-elem-quote layui-text">-->
<!--            注意事项-->
<!--        </blockquote>-->

        <?php echo $form->begin(); ?>
            <?php foreach ($form->getFields() as $field){ echo $field; } ?>
        <?php echo $form->end(); ?>



    </div>
</div>

<script>
    layui.use(<?php echo json_encode($form->getModules()); ?>, function () {
        <?php echo $form->getJsVarModules(); ?>
        /**
         * 初始化表单，要加上，不然刷新部分组件可能会不加载
         */
        form.render();

        //日期
        // laydate.render({
        //     elem: '#date'
        // });
        // laydate.render({
        //     elem: '#date1'
        // });

        //创建一个编辑器
        // var editIndex = layedit.build('LAY_demo_editor');

        <?php echo $form->getJsStr(); ?>

        //监听提交
        <?php echo $form->getJsSubmit(); ?>

        //表单初始赋值
       <?php echo $form->getJsDefaultValue(); ?>

    });
</script>
