<?php
use Catmes\LayAdmin\Components\Lay;
use Catmes\LayAdmin\Components\Template;
use Catmes\LayAdmin\Components\LoginPage;

/* @var LoginPage $loginPage */

$lay = Lay::getInstance();
$bgImage = $lay->getResourcePath()."/images/bg.jpg";

$title = $loginPage->getTitle();

$tpl = Template::getInstance();
$csrfTokenField = $tpl->getCsrfTokenField();
$csrfToken = $tpl->getCsrfToken();

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo $title; ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Access-Control-Allow-Origin" content="*">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <?php foreach ($lay->getCssFiles() as $cssFile): ?>
        <link rel="stylesheet" href="<?php echo $cssFile; ?>" media="all">
    <?php endforeach; ?>
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        body {background-image:url("<?php echo $bgImage; ?>");height:100%;width:100%;}
        #container{height:100%;width:100%;}
        input:-webkit-autofill {-webkit-box-shadow:inset 0 0 0 1000px #fff;background-color:transparent;}
        .admin-login-background {width:300px;height:300px;position:absolute;left:50%;top:40%;margin-left:-150px;margin-top:-100px;}
        .admin-header {text-align:center;margin-bottom:20px;color:#ffffff;font-weight:bold;font-size:30px}
        .admin-input {border-top-style:none;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;height:50px;width:300px;padding-bottom:0px;}
        .admin-input::-webkit-input-placeholder {color:#a78369}
        .layui-icon-circle {color:#a78369 !important;}
        .layui-icon-circle:hover {color:#9dadce !important;}
        .layui-icon-username {color:#a78369 !important;}
        .layui-icon-username:hover {color:#9dadce !important;}
        .layui-icon-password {color:#a78369 !important;}
        .layui-icon-password:hover {color:#9dadce !important;}
        .admin-input-username {border-top-style:solid;border-radius:10px 10px 0 0;}
        .admin-input-verify {border-radius:0 0 10px 10px;}
        .admin-button {margin-top:20px;font-weight:bold;font-size:18px;width:300px;height:50px;border-radius:5px;background-color:#a78369;border:1px solid #d8b29f}
        .admin-icon {margin-left:260px;margin-top:10px;font-size:30px;}
        i {position:absolute;}
        .admin-captcha {position:absolute;margin-left:205px;margin-top:-40px;}
    </style>
</head>
<body>
<div id="container layui-anim layui-anim-upbit">
    <div></div>
    <div class="admin-login-background">
        <div class="admin-header">
            <span><?php echo $title; ?></span>
        </div>
        <form class="layui-form" action="<?php echo $loginPage->getLoginActionUrl(); ?>" method="POST">
            <input type="hidden" name="<?php echo $csrfTokenField; ?>" value="<?php echo $csrfToken; ?>" class="layui-input">
            <?php echo $loginPage->getFieldUsername(); ?>
            <?php echo $loginPage->getFieldEmail(); ?>
            <?php echo $loginPage->getFieldPassword(); ?>
            <?php echo $loginPage->getSubmitBtn(); ?>

<!--            <div>-->
<!--                <input type="text" name="captcha" placeholder="请输入验证码" autocomplete="off" class="layui-input admin-input admin-input-verify" value="xszg">-->
<!--                <img class="admin-captcha" width="90" height="30" src="../images/captcha.jpg">-->
<!--            </div>-->

        </form>
    </div>
</div>
<?php foreach ($lay->getJsFiles() as $jsFile): ?>
    <script src="<?php echo $jsFile ?>" charset="utf-8"></script>
<?php endforeach; ?>
<script>
    layui.use(['form'], function () {
        var form = layui.form,
            layer = layui.layer;

        // 登录过期的时候，跳出ifram框架
        if (top.location != self.location) top.location = self.location;

        // 进行登录操作
        // form.on('submit(login)', function (data) {
        //     data = data.field;
        //     console.log(data)
        //     if (data.username == '') {
        //         layer.msg('用户名不能为空');
        //         return false;
        //     }
        //     if (data.password == '') {
        //         layer.msg('密码不能为空');
        //         return false;
        //     }
        //     // if (data.captcha == '') {
        //     //     layer.msg('验证码不能为空');
        //     //     return false;
        //     // }
        //     // layer.msg('登录成功', function () {
        //     //     window.location = '../index.html';
        //     // });
        //     return false;
        // });
    });
</script>
</body>
</html>
