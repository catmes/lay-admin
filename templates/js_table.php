<?php
use Catmes\LayAdmin\Components\LayJs\JsTable;
use Catmes\LayAdmin\Components\LayJs\JsAjax;
use Catmes\LayAdmin\Components\Template;

$csrfToken = Template::getInstance()->getCsrfToken();

/* @var JsTable $jsTable */

$table = $jsTable->getTable();
$layFilterId = $table->getLayFilterId();
$tableId = $table->getTableId();
$operates = $table->getOperates();
//echo (new JsAjax("/llkjk",['id'=>'data.id', '_token'=>'csrf_tokennnnnn']))->render();
?>

<script>
    layui.use(<?php echo json_encode($jsTable->getModules()); ?>, function () {
        <?php echo $jsTable->getJsVarModules(); ?>

        form.render();

        <?php echo $jsTable->getJsStr(); ?>

        table.render({
            elem: '#<?php echo $tableId; ?>',
            url: '<?php echo $table->getDataUrl(); ?>',
            toolbar: '#toolbarDemo',
            defaultToolbar: ['filter', 'exports', 'print', {
                title: '提示',
                layEvent: 'LAYTABLE_TIPS',
                icon: 'layui-icon-tips'
            }],
            cols: [<?php echo json_encode($table->getCols()); ?>],
            limits: [3, 10, 15, 20, 25, 50, 100],
            limit: 15,
            page: true,
            skin: 'line'
        });

        // 监听重置操作
        form.on('submit(data-reset-btn)', function (data) {
            //执行搜索重载
            table.reload('<?php echo $tableId; ?>', {
                page: {
                    curr: 1
                }
                , where: {
                    searchParams: {}
                }
            }, 'data');
            return false;
        });

        // 监听搜索操作
        form.on('submit(data-search-btn)', function (data) {
            var result = JSON.stringify(data.field);

            //执行搜索重载
            table.reload('<?php echo $tableId; ?>', {
                page: {
                    curr: 1
                }
                , where: {
                    searchParams: result
                }
            }, 'data');

            return false;
        });

        /**
         * toolbar事件监听
         */
        table.on('toolbar(<?php echo $layFilterId; ?>)', function (obj) {
            <?php foreach ($table->getToolbarButtons() as $btn){ echo $btn->renderJs(); } ?>
        });

//监听表格复选框选择
        table.on('checkbox(<?php echo $layFilterId; ?>)', function (obj) {
            console.log(obj)
        });

// 行操作 编辑 删除
        <?php foreach ($operates as $operate): ?>
        table.on('tool(<?php echo $layFilterId; ?>)', function (obj) {
            var data = obj.data;
            <?php foreach ($operate->getButtons() as $button): ?>
            <?php echo $button->renderJs(); ?>
            <?php endforeach; ?>
        });
        <?php endforeach; ?>

    });
</script>
