<?php

return [
    [
        'title'=>'常规管理','href'=>'','icon'=>'fa fa-address-book', 'target'=>'_self',
        'child'=>[
            ['title'=>'用户列表','href'=>'/admin/users','icon'=>'fa fa-users','target'=>'_self'],
            ['title'=>'视频列表','href'=>'/admin/videos','icon'=>'fa fa-navicon','target'=>'_self'],
            ['title'=>'新手视频库','href'=>'/admin/videos/weight-list','icon'=>'fa fa-navicon','target'=>'_self'],
            ['title'=>'APP上传','href'=>'/admin/app/releases','icon'=>'fa fa-navicon','target'=>'_self'],
        ]
    ]
];
