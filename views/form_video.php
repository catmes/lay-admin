<?php
use Catmes\LayAdmin\Components\LayJs\JsAjax;

$jsAjax = new JsAjax('/admin/videos/edit', ['id'=>$model->id,'data'=>'data.field']);
$jsAjax->setJsVars(['data.field'])->setSuccess(JsAjax::JS_RESPONSE."layer.close(parentIndex);");

?>
<div class="layuimini-main">

    <div class="layui-form layuimini-form">

        <div class="layui-form-item">
            <label class="layui-form-label required">标题</label>
            <div class="layui-input-block">
                <input type="text" name="title" lay-verify="required" lay-reqtext="用户名不能为空" placeholder="请输入用户名" value="<?php echo $model->title; ?>" class="layui-input">
                <tip>更改视频标题</tip>
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label required">是否推荐</label>
            <div class="layui-input-block">
                <input type="radio" name="is_recommend" value=1 title="是" <?php if($model->is_recommend==1){echo "checked"; }?>>
                <input type="radio" name="is_recommend" value=0 title="否" <?php if($model->is_recommend==0){echo "checked"; }?>>
            </div>
        </div>

        <!--
        <div class="layui-form-item">
            <label class="layui-form-label required">手机</label>
            <div class="layui-input-block">
                <input type="number" name="phone" lay-verify="required" lay-reqtext="手机不能为空" placeholder="请输入手机" value="" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">邮箱</label>
            <div class="layui-input-block">
                <input type="email" name="email" placeholder="请输入邮箱" value="" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">职业</label>
            <div class="layui-input-block">
                <input type="text" name="work" placeholder="请输入职业" value="" class="layui-input">
            </div>
        </div>


        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">备注信息</label>
            <div class="layui-input-block">
                <textarea name="remark" class="layui-textarea" placeholder="请输入备注信息"></textarea>
            </div>
        </div>

        --->

        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn layui-btn-normal" lay-submit lay-filter="saveBtn">确认保存</button>
            </div>
        </div>
    </div>
</div>
<script>
    layui.use(['form', 'table'], function () {
        var form = layui.form,
            layer = layui.layer,
            table = layui.table,
            $ = layui.$;

        /**
         * 初始化表单，要加上，不然刷新部分组件可能会不加载
         */
        form.render();

        // 当前弹出层，防止ID被覆盖
        var parentIndex = layer.index;

        //监听提交
        form.on('submit(saveBtn)', function (data) {
            <?php echo $jsAjax; ?>
            // table.render().reload();
            return false;
        });

    });
</script>
