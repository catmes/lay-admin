<?php


namespace Catmes\LayAdmin\Components;


class TableData
{
    protected $code=0;
    protected $count=0;
    protected $msg='';
    protected $data=[];

    public function __construct($items, $total=null){
        if($total===null){
            $total = count($items);
        }
        $this->count = $total;
        $this->data = $items;
    }

    public function toArray():array{
        return ['code'=>$this->code, 'count'=>$this->count, 'data'=>$this->data, 'msg'=>$this->msg];
    }
}
