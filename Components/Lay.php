<?php


namespace Catmes\LayAdmin\Components;


/**
 * 基于Layuimini封装的后台管理系统
 * 下载前端资源 git clone https://gitee.com/zhongshaofa/layuimini -b v2-onepage
 * @link http://layuimini.99php.cn/
 */
class Lay
{

    protected static $instance;

    protected $title='layuimini后台管理系统';
    protected $iniUrl = '/admin/layInit';
    protected $clearUrl = '/admin/layClear';
    protected $homeUrl = '/resources/layuimini/page/welcome-1.html';
    protected $logoutUrl = '/logout';
    protected $username='admin';

    protected $menu;

    /* @var Resource $resource */
    protected $resource;

    protected function __construct(){
        $this->resource = Resource::getInstance();
        $this->menu = new Menu();
    }

    /**
     * 调用此静态方法获得对象单例。 避免多次实例化。
     * 一次实例化，即可多处复用。
     */
    public static function getInstance() :self
    {
        if(!self::$instance instanceof self){
            // 单例模式故，此代码一个页面生命周期内仅执行一次。
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function getUsername():string{
        return $this->username;
    }
    public function setUsername(string $username):self{
        $this->username = $username;
        return $this;
    }

    public function setHomeUrl($url) :self {
        $this->homeUrl = $url;
        return $this;
    }

    public function setIniUrl($url):self{
        $this->iniUrl = $url;
        return $this;
    }
    public function setClearUrl($url):self{
        $this->clearUrl = $url;
        return $this;
    }
    public function getClearResult($msg='服务端缓存清理成功', $code=1):array{
        return ['code'=>$code,'msg'=>$msg];
    }

    public function getMenu() :Menu {
        return $this->menu;
    }

    public function getInitData() :array {
        $homeInfo = ['title'=>$this->title,'href'=>$this->homeUrl];
        $logoInfo = $this->menu->getLogoInfo();
        $menuInfo = $this->menu->getMenuInfo();
        return compact('homeInfo', 'logoInfo', 'menuInfo');
    }

    public function getIniUrl(){
        return $this->iniUrl;
    }
    public function getClearUrl(){
        return $this->clearUrl;
    }
    public function setTitle($title){
        $this->title = $title;
        return $this;
    }

    public function getTitle(){
        return $this->title;
    }

    public function setLogoutUrl($url):self{
        $this->logoutUrl = $url;
        return $this;
    }

    public function getLogoutUrl():string{
        return $this->logoutUrl;
    }

    public function getResourcePath() :string {
        return $this->resource->getPath();
    }

    public function getCssFiles():array{
        return $this->resource->getCssFiles();
    }

    public function getJsFiles():array{
        return $this->resource->getJsFiles();
    }

    public function render(){
        $tpl = Template::getInstance();
        $tplFile = $tpl->getFrameFile();
        return $tpl->render($tplFile, ['lay'=>$this]);
    }


}
