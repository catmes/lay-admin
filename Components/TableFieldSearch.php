<?php

namespace Catmes\LayAdmin\Components;


use Catmes\LayAdmin\Components\LayJs\JsPage;
use Catmes\LayAdmin\helpers\HtmlHelper;

class TableFieldSearch
{
    const TYPE_TEXT = 'text';
    const TYPE_SELECT = 'select';
    const TYPE_CHECKBOX = 'checkbox';
    const TYPE_TIME_RANGE = 'time_range';

    protected $inputType=self::TYPE_TEXT;

    protected $field;
    protected $label;
    protected $value=null;
    protected $fieldHtml;
    protected $options=[];
    protected $id='search_id';

    public function __construct(string $field, $label='')
    {
        $this->field = $field;
        $this->label = $label;
    }

    public function setOptions($options=[]):self{
        $this->options = array_merge($this->options, $options);
        return $this;
    }
    public function setValue($value) :self {
        $this->value = $value;
        return $this;
    }

    public function setLabel($label) :self {
        $this->label = $label;
        return $this;
    }

    public function render(){
        if(empty($this->fieldHtml)){
            $this->searchTextInput();
        }
        return $this->fieldHtml;
    }

    public function __toString(){
        return $this->render();
    }
    protected function getId():string{
        return $this->id . mt_rand(11111,99999);
    }

    /*
    public function searchTimeRange($startField='start_time', $endField='end_time'):self{
        $jsPage = JsPage::getInstance();
        $jsPage->addModule('laydate');
        $eleId = $this->getId();
        $startInputId = 'startDate';
        $endInputId = 'endDate';
        $jsStr = <<<JSSTR
  laydate.render({
    elem: '#{$eleId}'
    //设置开始日期、日期日期的 input 选择器
    // ,range: ['#{$startInputId}', '#{$endInputId}'] 数组格式为 2.6.6 开始新增，之前版本直接配置 true 或任意分割字符即可
    ,range: true
    ,type: 'datetime'
    ,trigger: 'click'
  });
JSSTR;

        $jsPage->addJsStr($jsStr);
        $labelHtml = empty($this->label) ? '' : self::label($this->label);
        $nowTime = ''; // date('Y-m-d H:i:s', time());
        $inputOpts = ['autocomplete'=>'off', 'class'=>'layui-input'];
        // // '<input type="text" autocomplete="off" id="'.$startInputId.'" class="layui-input" placeholder="开始日期">';
        $startInput = HtmlHelper::textInput($startField, $nowTime, array_merge($inputOpts,['id'=>$startInputId]));
        $startDiv = self::divInputInline($startInput);
        $line = '<div class="layui-form-mid">-</div>';
        // '<input type="text" autocomplete="off" id="'.$endInputId.'" class="layui-input" placeholder="结束日期">';
        $endInput = HtmlHelper::textInput($endField, $nowTime, array_merge($inputOpts,['id'=>$endInputId]));
        $endDiv = self::divInputInline($endInput);
        $inputDiv = self::divInline($startDiv.$line.$endDiv, ['id'=>$eleId]);
        $this->fieldHtml = self::divInline($labelHtml. $inputDiv);
        return $this;
    }
*/

    public function searchTextInput($options=[]) :self {
        $labelHtml = empty($this->label) ? '' : self::label($this->label);
        $options = array_merge(['class'=>'layui-input','autocomplete'=>'off'], $options);
        $this->options = array_merge($this->options, $options);
        $divOptions = ['class'=>'layui-input-inline'];
        $textInput = HtmlHelper::textInput($this->field, $this->value, $this->options);
        $inputHtml = self::div($textInput,$divOptions);
        $this->fieldHtml = self::divInline($labelHtml.$inputHtml);
        return $this;
    }

    public function searchTimeInput($options=[]):self{
        $eleId = $this->getId();
        $jsPage = JsPage::getInstance();
        $jsPage->addModule('laydate');
        $jsStr = <<<JSSTR
  laydate.render({
    elem: '#{$eleId}'
    ,type: 'datetime'
    ,trigger: 'click'
  });
JSSTR;

        $jsPage->addJsStr($jsStr);
        $options['id'] = $eleId;
        return $this->searchTextInput($options);
    }

    public function searchSelectInput(array $items, $cssClasses=[]):self{
        $labelHtml = empty($this->label) ? '' : self::label($this->label);
        $this->options['lay-filter'] = 'aihao';
        HtmlHelper::addCssClass($this->options, $cssClasses);
        $inputHtml = HtmlHelper::dropDownList($this->field, $this->value, $items, $this->options);
        $inputHtml = self::divInputInline($inputHtml);
        $this->fieldHtml = self::divInline($labelHtml.$inputHtml);
        return $this;
    }

    protected static function divInline($content, $options=[]):string{
        $options = array_merge(['class'=>'layui-inline'],$options);
        return self::div($content, $options);
    }
    protected static function divInputInline($content):string{
        return self::div($content,['class'=>'layui-input-inline']);
    }

    protected static function label($content, $options=[]):string
    {
        $defaultOptions = ['class'=>'layui-form-label'];
        $options = array_merge($defaultOptions, $options);
        return HtmlHelper::label($content, null, $options);
    }

    protected static function div($content, $options=[]):string{
        return HtmlHelper::tag('div', $content, $options);
    }

}
