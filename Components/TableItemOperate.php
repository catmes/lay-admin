<?php


namespace Catmes\LayAdmin\Components;


class TableItemOperate extends TableColumn
{
    /* @var TableItemOperateBtn[] $buttons */
    protected $buttons = [];
    protected $itemToolbarId = 'currentTableBar';
    protected $eventName;

    public function __construct(Table $table, $config=[]){
        parent::__construct($table, $config);
        $this->itemToolbarId = 'currentTableBar'. mt_rand(11111,99999);
        $this->setAttribute('toolbar', "#".$this->itemToolbarId);
    }

    private function getEventName():string{
        return '';
//        if(!$this->eventName){
//            $this->eventName = "eventName".mt_rand(11111,99999);
//        }
//        return $this->eventName;
    }

    public function getItemToolbarId():string{
        return $this->itemToolbarId;
    }

    public function getTable():Table{
        return $this->table;
    }

    public function getButtons():array{
        return $this->buttons;
    }

    public function setBtnGet($url, $title, $cssClasses=[], $event=''):TableItemOperateBtn{
        $btn = new TableItemOperateBtn($this,$url);
        $event = $event ?: $this->getEventName();
        $btn->btnGet($title,$cssClasses,$event);
        $this->buttons[] = $btn;
        return $btn;
    }

    public function setBtnPost($url, $title, $cssClasses=[], $event=''):TableItemOperateBtn{
        $btn = new TableItemOperateBtn($this,$url);
        $event = $event ?: $this->getEventName();
        $btn->btnPost($title,$cssClasses,$event);
        $this->buttons[] = $btn;
        return $btn;
    }

    public function setEditBtn($url):TableItemOperateBtn{
        return $this->setBtnGet($url,'编辑',['layui-btn-normal','data-count-edit'],'edit');
    }

    public function setDeleteBtn($url):TableItemOperateBtn{
        return $this->setBtnPost($url, '删除',['layui-btn-danger','data-count-delete'],'delete');
    }

}
