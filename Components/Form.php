<?php


namespace Catmes\LayAdmin\Components;


use Catmes\LayAdmin\Components\LayJs\JsAjax;

class Form
{
    /* @var FormField[] $fields */
    protected $fields = [];

    protected $actionUrl='';

    protected $defaultValues=[];

    protected $jsStr='';

    protected $modules=['jquery','form', 'layedit', 'laydate','layer'];

    protected $templateFile = 'form.php';

    // 单页应用不支持相对路径
    public function __construct($actionUrl=""){
        $this->actionUrl = $actionUrl;
    }

    public function getModules():array{
        return $this->modules;
    }

    public function addModule(string $module):self{
        $this->modules[] = $module;
        return $this;
    }
    public function addJs(string $jsStr):self{
        $this->jsStr .= $jsStr;
        return $this;
    }
    public function getJsStr():string{
        return $this->jsStr;
    }

    public function getJsVarModules():string{
        $jsStr = '';
        foreach ($this->modules as $module){
            if($module=='jquery'){
                $jsStr .= "var $ = layui.{$module};";
            }
            $jsStr .= "var {$module} = layui.{$module};";
        }
        return $jsStr;
    }

    public function field($name, $value=null):FormField{
        $field = new FormField($name, $value);
        $field->setForm($this);
        $this->setValueForJs($name, $value);
        $this->fields[] = $field;
        return $field;
    }

    public function submit($options=[]):FormField{
        $options = array_merge(['lay-filter'=>$this->getSubmitFilterId()],$options);
        $submit = (new FormField())->submit('提交',$options);
        $this->fields[] = $submit;
        return $submit;
    }

    public function begin():string{
        $url = $this->actionUrl;
        $fid = $this->getFilterId();
        return "<form class=\"layui-form\" action=\"{$url}\" lay-filter=\"{$fid}\">";
    }
    public function end():string{
        return "</form>";
    }

    protected function setValueForJs($key, $value):self{
        $this->defaultValues[$key] = $value;
        return $this;
    }

    public function getFields():array{
        return $this->fields;
    }

    public function render():string{
        return Template::getInstance()->render($this->templateFile, ['form'=>$this]);
    }

    public function __toString(){
        return $this->render();
    }

    public function getFilterId():string{
        return 'example';
    }

    public function getSubmitFilterId():string{
        return 'demo1';
    }

    public function getJsDefaultValue():string{
        $defaultValues = json_encode($this->defaultValues);
        $formFilterId = $this->getFilterId();
        return <<<JSSTR
        form.val('{$formFilterId}', {$defaultValues})
JSSTR;

    }

    public function getJsValue():string{
        $formFilterId = $this->getFilterId();
        return "form.val('{$formFilterId}')";
    }


    public function getJsSubmit():string{
        $submitFilterId = $this->getSubmitFilterId();
        $ajax = new JsAjax($this->actionUrl,['data'=>'data.field']);
        $jsCloseParentLayer = "layer.close(parentIndex);";
//        $this->modules[] = 'table';
//        $jsTableReload = "table.render().reload();";
        $ajax->setJsVars(['data.field'])->setSuccess(JsAjax::JS_RESPONSE.$jsCloseParentLayer);
        return <<<JSSTR
        var layer = layui.layer;
        // 当前弹出层，防止ID被覆盖
        var parentIndex = layer.index;
        form.on('submit({$submitFilterId})', function (data) {
            console.log(data)
            {$ajax}
            return false;
        });
JSSTR;

    }

}
