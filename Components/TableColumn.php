<?php


namespace Catmes\LayAdmin\Components;


class TableColumn
{

    /* @var array $attributes */
    protected $attributes;

    /* @var Table $table */
    protected $table;

    public function __construct(Table $table, $config=[]){
        $this->table = $table;
        $this->attributes = $config;
    }

    protected function setAttribute(string $attr, $value):self{
        $this->attributes[$attr] = $value;
        return $this;
    }

    public function getAttributes():array{

        return $this->attributes;
    }

}
