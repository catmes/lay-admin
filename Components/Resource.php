<?php


namespace Catmes\LayAdmin\Components;

/**
 * 下载前端资源 git clone https://gitee.com/zhongshaofa/layuimini -b v2-onepage
 */
class Resource
{
    protected static $instance;

    protected $basePath = '/resources/layuimini';
    protected $cssFiles = [
        '/lib/layui-v2.5.5/css/layui.css',
        '/lib/font-awesome-4.7.0/css/font-awesome.min.css',
        '/css/layuimini.css?v=2.0.1',
        '/css/themes/default.css',
        '/css/public.css'
    ];

    protected $jsFiles = [
        '/lib/layui-v2.5.5/layui.js',
        '/js/lay-config.js?v=2.0.0'
    ];

    protected function __construct(){
//        $remotefile = $_SERVER['HTTP_HOST'].$this->basePath.$this->cssFiles[0];
//        $response = @file_get_contents($remotefile); // @get_headers($remotefile, 1);
    }

    /**
     * 调用此静态方法获得对象单例。 避免多次实例化。
     * 一次实例化，即可多处复用。
     */
    public static function getInstance() :self
    {
        if(!self::$instance instanceof self){
            // 单例模式故，此代码一个页面生命周期内仅执行一次。
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function getPath() :string {
        return $this->basePath;
    }

    public function getCssFiles():array{
        foreach ($this->cssFiles as &$cssFile){
            $cssFile = $this->basePath . $cssFile;
        }
        return $this->cssFiles;
    }

    public function getJsFiles():array{
        foreach ($this->jsFiles as &$jsFile){
            $jsFile = $this->basePath . $jsFile;
        }
        return $this->jsFiles;
    }

}
