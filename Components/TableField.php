<?php

namespace Catmes\LayAdmin\Components;

class TableField extends TableColumn
{

    /* @var TableFieldSearch $search */
    protected $search;

    public function __construct(Table $table, $config=[]){
        $default = ['minWidth'=>80];
        $config = array_merge($default, $config);
        parent::__construct($table, $config);
    }


    public function setHide():self{
        return $this->setAttribute('hide', true);
    }

    public function setSort():self{
        return $this->setAttribute('sort', true);
    }

    public function setWidth($width):self{
        return $this->setAttribute('width', $width);
    }
    public function setMinWidth($minWidth):self{
        return $this->setAttribute('minWidth', $minWidth);
    }

    public function search() :TableFieldSearch {
        $field = $this->attributes['field'];
        $label = $this->attributes['title'];
        $search = new TableFieldSearch($field, $label);
        $this->search = $search;
        $this->table->setSearchField($this->search);
        return $this->search;
    }

}
