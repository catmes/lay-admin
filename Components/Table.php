<?php

namespace Catmes\LayAdmin\Components;

use Catmes\LayAdmin\helpers\HtmlHelper;
use Catmes\LayAdmin\Components\LayJs\JsTable;

class Table
{

    protected $templateFile="table.php";

    /* @var Template $template */
    protected $template;

    /* @var TableField[] $fields */
    protected $fields;

    /* @var TableItemOperate[] $operates */
    protected $operates=[];

    /* @var TableColumn[] $columns */
    protected $columns;

    /* @var TableFieldSearch[] $searches */
    protected $searches=[];

    /* @var JsTable $layJs */
    protected $layJs;

    protected $dataUrl='';

    /* @var TableToolbarBtn[] $toolBarButtons */
    protected $toolBarButtons = [];

    public function __construct($dataUrl)
    {
        $this->template = Template::getInstance();
        $this->dataUrl = $dataUrl;
    }

    public function checkbox($width=50, $fixed=true) :TableColumn {
        $config = ['type'=>'checkbox', 'width'=>$width, 'fixed'=>$fixed];
        return $this->column($config);
    }

    public function getDataUrl():string{
        return $this->dataUrl;
    }

    protected function createToolbarBtn($url, \Closure $closure, $toLayUrl=true):self{
        $url = $toLayUrl ? LayUri::getUrl($url) : $url;
        $btn = new TableToolbarBtn($this, $url);
        $closure($btn);
        $this->toolBarButtons[] = $btn;
        return $this;
    }

    public function addBtnRedirectToUrl($url, $content, $cssClasses=[]):self{
        return $this->createToolbarBtn($url, function (TableToolbarBtn $btn) use($content, $cssClasses){
            $btn->btnRedirectToUrl($content, $cssClasses);
        });
    }

    public function addBtnOpenByUrl($url, $content, $cssClasses=[]):self{
        return $this->createToolbarBtn($url, function (TableToolbarBtn $btn)use($content, $cssClasses){
            $btn->btnOpenByUrl($content, $cssClasses);
        }, false);
    }

    public function addBtnPostToUrl($url, $content, $cssClasses=[]):self{
        return $this->createToolbarBtn($url, function (TableToolbarBtn $btn)use($content, $cssClasses){
            $btn->btnPostToUrl($content, $cssClasses);
        }, false);
    }

    public function getToolbarButtons():array{
        return $this->toolBarButtons;
    }

    protected function column(array $config) :TableColumn {
        $col = new TableColumn($this, $config);
        $this->columns[] = $col;
        return $col;
    }

    public function field($field, $title='') :TableField {
        $config = compact('field', 'title');
        $field = new TableField($this, $config);
        $this->columns[] = $field;
        $this->fields[] = $field;
        return $field;
    }

    public function getLayFilterId():string{
        return "currentTableFilter";
    }

    public function getTableId():string{
        return "currentTableId";
    }

    public function getJsReload():string{
        // return "table.reload('{$tableId}',{url:'{$this->getDataUrl()}',where:{}});";
        return "table.reload(\"{$this->getTableId()}\");";
    }

    public function itemOperate(\Closure $closure, $title='操作') :TableItemOperate {
        $config = ['title'=>$title,'minWidth'=>90, 'align'=>'center'];
        $operate = new TableItemOperate($this, $config);
        $closure($operate);
        $this->columns[] = $operate;
        $this->operates[] = $operate;
        return $operate;
    }

    public function getOperates():array{
        return $this->operates;
    }

    public function setSearchField(TableFieldSearch $search) :self {
        $this->searches[] = $search;
        return $this;
    }

    public function getFieldSearches() :array {
        return $this->searches;
    }

    public function render(){
        if($this->dataUrl==''){
            $msg = 'dataUrl未设置.请执行setDataUrl()方法设置数据源';
            return "{'code':1,'msg':'{$msg}'}";
        }
        $this->layJs = new JsTable($this);
        return $this->template->render($this->templateFile, ['table'=>$this]);
    }

    public function getCols():array{
        $cols = [];
        foreach ($this->columns as $column){
            $cols[] = $column->getAttributes();
        }
        return $cols;
    }

    public function getLayJs():JsTable{
        return $this->layJs;
    }

}
