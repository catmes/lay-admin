<?php


namespace Catmes\LayAdmin\Components;


class UploadMulti
{
    /* @var Form $form */
    protected $form;

    protected $url;

    /* @var string $acceptFileType 指定允许上传时校验的文件类型，可选值有：images（图片）、file（所有文件）、video（视频）、audio（音频） */
    protected $acceptFileType=self::ACCEPT_FILE;

    const ACCEPT_FILE = 'file';
    const ACCEPT_IMAGE = 'images';
    const ACCEPT_VIDEO = 'video';
    const ACCEPT_AUDIO = 'audio';

    protected $templateFile='upload_multi.php';

//    protected $jsStr='';

    public function __construct($url, $acceptFileType=self::ACCEPT_FILE)
    {
        $this->url = $url;
        $this->acceptFileType = $acceptFileType;
    }

    public function getAcceptFileType():string{
        return $this->acceptFileType;
    }

    public function setForm(Form $form):self{
        $this->form = $form;
        return $this;
    }

    public function getForm():Form{
        return $this->form;
    }

    public function getUrl():string {
        return $this->url;
    }

    public function getUploadBtnId():string {
        return 'upload-btn-id-4837918749';
    }

//    public function addJs($content):self {
//        $this->jsStr .= $content;
//        return $this;
//    }
//    public function getJsStr():string {
//        return $this->jsStr;
//    }

    public function render():string {
        $template = Template::getInstance();
        return $template->render($this->templateFile,['uploadMulti'=>$this]);
    }

    public function __toString(){
        return $this->render();
    }

}
