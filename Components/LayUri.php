<?php


namespace Catmes\LayAdmin\Components;


class LayUri
{
    /**
     * Layuimini URL跳转规则
     * [推荐] 绝对路径基于当前域名或http. 绝对路径字符串以斜杠 "/" 或 "http" 开头
     * 相对路径是基于 "/admin" 路径. 相对路径字符串不以斜杠 "/" 开头
     */
    public static function getUrl($url=''):string{
        return "#/".$url;
//        if(strpos($url, 'http')===0){}
//        if(strpos($url, '/')===0){}

    }
}
