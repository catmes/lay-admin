<?php


namespace Catmes\LayAdmin\Components;


use Catmes\LayAdmin\helpers\HtmlHelper;

class LoginPage
{
    protected $templateFile = 'login.php';

    protected $loginActionUrl = '';

    protected $submitBtn="<button class=\"layui-btn admin-button\" lay-submit=\"\" lay-filter=\"login\">登 录</button>";

    protected $fieldUsername=[];
    protected $fieldPassword=[];
    protected $fieldEmail=[];

    protected $title;
    public function setTitle($title):self{
        $this->title = $title;
        return $this;
    }

    public function getTitle():string{
        if(empty($this->title)){
            $lay = Lay::getInstance();
            $this->title = $lay->getTitle()."-后台管理系统";
        }
        return $this->title;
    }

    public function setSubmitBtn($content, $options=[]):self{
        $defOpts = ['class'=>['layui-btn','admin-button'], 'lay-submit'=>"", 'lay-filter'=>'login'];
        $options = array_merge($defOpts, $options);
        $this->submitBtn = HtmlHelper::tag('button',$content, $options);
        return $this;
    }
    public function getSubmitBtn():string{
        return $this->submitBtn;
    }

    public function getFieldEmail():string{
//        $defaultHtml = <<<HTML
//            <div>
//                <i class="layui-icon layui-icon-username admin-icon"></i>
//                <input type="text" name="email" placeholder="请输入邮箱" autocomplete="off" class="layui-input admin-input admin-input-username">
//            </div>
//HTML;
        if($this->fieldEmail == []){
            return '';
        }
        $defInputOptions = ['placeholder'=>'请输入邮箱', 'autocomplete'=>'off', 'class'=>'layui-input admin-input admin-input-username'];
        $options = array_merge($defInputOptions, $this->fieldUsername['options']);
        $inputHtml = HtmlHelper::textInput($this->fieldEmail['name'], null, $options);
        $iconHtml = "<i class=\"layui-icon layui-icon-circle admin-icon\"></i>";
        return "<div>".$iconHtml.$inputHtml."</div>";
    }
    public function setFieldEmail(string $fieldName,$options=[]):self{
        $this->fieldEmail = ['name'=>$fieldName, 'options'=>$options];
        return $this;
    }

    public function getFieldUsername():string{
        $defaultHtml = <<<HTML
            <div>
                <i class="layui-icon layui-icon-username admin-icon"></i>
                <input type="text" name="username" placeholder="请输入用户名" autocomplete="off" class="layui-input admin-input admin-input-username">
            </div>
HTML;
        if($this->fieldUsername == []){
            return $defaultHtml;
        }
        $defInputOptions = ['placeholder'=>'请输入登录名', 'autocomplete'=>'off', 'class'=>'layui-input admin-input admin-input-username'];
        $options = array_merge($defInputOptions, $this->fieldUsername['options']);
        $inputHtml = HtmlHelper::textInput($this->fieldUsername['name'], null, $options);
        $iconHtml = "<i class=\"layui-icon layui-icon-username admin-icon\"></i>";
        return "<div>".$iconHtml.$inputHtml."</div>";
    }
    public function setFieldUsername(string $fieldName,$options=[]):self{
        $this->fieldUsername = ['name'=>$fieldName, 'options'=>$options];
        return $this;
    }


    public function getFieldPassword():string{
        $defaultHtml = <<<HTML
            <div>
                <i class="layui-icon layui-icon-password admin-icon"></i>
                <input type="password" name="password" placeholder="请输入密码" autocomplete="off" class="layui-input admin-input">
            </div>
HTML;
        if($this->fieldPassword == []){
            return $defaultHtml;
        }
        $defInputOptions = ['placeholder'=>'请输入密码','autocomplete'=>'off', 'class'=>'layui-input admin-input'];
        $options = array_merge($defInputOptions, $this->fieldPassword['options']);
        $inputHtml = HtmlHelper::passwordInput($this->fieldPassword['name'], null, $options);
        $iconHtml = "<i class=\"layui-icon layui-icon-password admin-icon\"></i>";
        return "<div>".$iconHtml.$inputHtml."</div>";
    }
    public function setFieldPassword(string $fieldName,$options=[]):self{
        $this->fieldPassword = ['name'=>$fieldName, 'options'=>$options];
        return $this;
    }

    public function setLoginActionUrl($url):self{
        $this->loginActionUrl = $url;
        return $this;
    }

    public function getLoginActionUrl():string{
        return $this->loginActionUrl;
    }

    public function render():string{
        $template = Template::getInstance();
        return $template->render($this->templateFile,['loginPage'=>$this]);
    }

    public function __toString(){
        return $this->render();
    }
}
