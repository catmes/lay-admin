<?php


namespace Catmes\LayAdmin\Components\LayJs;


use Catmes\LayAdmin\helpers\ArrayHelper;

class JsOpenUrl
{
    /* @var string $url URL链接必须自己包含引号。如： $url = "\"/admin/videos/delete\""; */
    protected $url = '';
    protected $title = '';
    protected $endFunc = '';
    public function __construct($url, $title='窗口标题'){
        $this->url = $url;
        $this->title = $title;
    }
    protected function getJsStr():string{
        // URL链接必须自己包含引号。如： $url = "\"/admin/videos/delete\"";
        return <<<AJAXTPL
                var content = miniPage.getHrefContent({$this->url});
                var openWH = miniPage.getOpenWidthHeight();
                var index = layer.open({
                    title: '{$this->title}',
                    type: 1,
                    shade: 0.2,
                    maxmin: true,
                    shadeClose: true,
                    end: function(){{$this->endFunc}},
                    area: [openWH[0] + 'px', openWH[1] + 'px'],
                    offset: [openWH[2] + 'px', openWH[3] + 'px'],
                    content: content,
                });
                $(window).on("resize", function () {
                    layer.full(index);
                });
AJAXTPL;
    }

    public function setEndFunc($end):self{
        $this->endFunc = $end;
        return $this;
    }

    public function render():string{
        return $this->getJsStr();
    }

    public function __toString(){
        return $this->render();
    }


}
