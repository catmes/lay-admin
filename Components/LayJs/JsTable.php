<?php

namespace Catmes\LayAdmin\Components\LayJs;

use Catmes\LayAdmin\Components\Table;
use Catmes\LayAdmin\Components\Template;

class JsTable
{

    protected $modules=['form', 'table','miniPage', 'jquery'];

    protected $templateFile = 'js_table.php';

    /* @var Table $table */
    protected $table;

    protected $template;

    public function __construct(Table $table)
    {
        $this->template = Template::getInstance();
        $this->table = $table;
    }

    public function getModules():array{
        $jsPage = JsPage::getInstance();
        $pageModules = $jsPage->getModules();
        $jsPage->setModules(array_merge($pageModules, $this->modules));
        return $jsPage->getModules();
    }

    public function getJsVarModules():string{
        $jsPage = JsPage::getInstance();
        return $jsPage->getJsVarModules();
    }

    public function getJsStr():string{
        $jsPage = JsPage::getInstance();
        return $jsPage->getJsStr();
    }

    public function render():string{
        return $this->template->render($this->templateFile, ['jsTable'=>$this]);
    }

    public function getTable():Table{
        return $this->table;
    }

}
