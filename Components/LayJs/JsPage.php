<?php


namespace Catmes\LayAdmin\Components\LayJs;


class JsPage
{
    protected static $instance;
    protected function __construct(){}
    protected function __clone(){}
    public static function getInstance() :self
    {
        if(!self::$instance instanceof self){
            // 单例模式故，此代码一个页面生命周期内仅执行一次。
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected $modules=[];
    public function getModules():array{
        return $this->modules;
    }
    public function setModules(array $modules):self{
        $this->modules = $modules;
        return $this;
    }
    public function addModule(string $module):self{
        if(!in_array($module, $this->modules)){
            $this->modules[] = $module;
        }
        return $this;
    }

    protected $jsStr='';
    public function getJsStr():string{
        return $this->jsStr;
    }
    public function addJsStr(string $js):self{
        $this->jsStr .= $js;
        return $this;
    }
    public function getJsVarModules():string{
        $jsStr = '';
        foreach ($this->modules as $module){
            if($module=='jquery'){
                $jsStr .= "var $ = layui.{$module};";
            }else{
                $jsStr .= "var {$module} = layui.{$module};";
            }

        }
        return $jsStr;
    }

}
