<?php

namespace Catmes\LayAdmin\Components;

class Menu
{

    protected $logoInfo=[];
    protected $menuInfo;

    protected static $defaultSideMenus = [
        [
            'title'=>'主页模板','href'=>'','icon'=>'fa fa-home','target'=>'_self',
            'child'=>[
                ['title'=>'主页一','href'=>'/resources/layuimini/page/welcome-1.html','icon'=>'fa fa-tachometer','target'=>'_self'],
                ['title'=>'主页二','href'=>'/resources/layuimini/page/welcome-2.html','icon'=>'fa fa-tachometer','target'=>'_self'],
                ['title'=>'主页三','href'=>'/resources/layuimini/page/welcome-3.html','icon'=>'fa fa-tachometer','target'=>'_self'],
            ]
        ],
        [
            'title'=>'登录模板','href'=>'','icon'=>'fa fa-flag-o','target'=>'_self',
            'child'=>[
                ['title'=>'登录-1','href'=>'/resources/layuimini/page/login-1.html','icon'=>'fa fa-stumbleupon-circle','target'=>'_blank'],
                ['title'=>'登录-2','href'=>'/resources/layuimini/page/login-2.html','icon'=>'fa fa-viacoin','target'=>'_blank'],
                ['title'=>'登录-3','href'=>'/resources/layuimini/page/login-3.html','icon'=>'fa fa-tags','target'=>'_blank'],
            ]
        ],
        ['title'=>'菜单管理','href'=>'/resources/layuimini/page/menu.html','icon'=>'fa fa-window-maximize','target'=>'_self'],
        ['title'=>'表格示例','href'=>'/resources/layuimini/page/table.html','icon'=>'fa fa-file-text','target'=>'_self'],
    ];

    protected static function getDefaultMenuInfo() :array {
        return [
            [
                'title'=>'常规管理','href'=>'','icon'=>'fa fa-address-book', 'target'=>'_self',
                'child'=>self::$defaultSideMenus
            ]
        ];
    }

    protected static $defaultLogoInfo = [
            'title' => 'LAYUI MINI',
            'image' => "/resources/layuimini/images/logo.png",
            'href' => ""
    ];

    public function __construct(){

    }

    public function setMenuInfo($menuInfo){
        $this->menuInfo = $menuInfo;
        return $this;
    }

    public function setMenuByFile($menuFile){
        if(file_exists($menuFile)){
            $menuInfo = require $menuFile;
            $this->setMenuInfo($menuInfo);
        }
        return $this;
    }

    public function setLogoTitle($title){
        $this->logoInfo['title'] = $title;
        return $this;
    }
    public function setLogoImage($image){
        $this->logoInfo['image'] = $image;
        return $this;
    }

    public function getMenuInfo() :array {
        return $this->menuInfo ?: self::getDefaultMenuInfo();
    }

    public function getLogoInfo() :array {
        return array_merge(self::$defaultLogoInfo, $this->logoInfo);
    }


}
