<?php

namespace Catmes\LayAdmin\Controllers;

use App\Exceptions\ExceptionApi;
use Catmes\LayAdmin\Components\Lay;
use App\Services\Response as ApiResponse;
use Catmes\LayAdmin\Components\Template;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Http\Response;

class BaseController extends Controller
{

    public function __construct()
    {
        $lay = Lay::getInstance()->setTitle('抖个球');
        Template::getInstance()->setViewPath(dirname(__DIR__)."/views");
        $lay->setHomeUrl("/admin/videos");
        $lay->setLogoutUrl("/logout");
        $menu = $lay->getMenu();
        $menu->setMenuByFile(dirname(__DIR__)."/menu.php");
        $menu->setLogoTitle('抖个球');
    }

    protected function successApi($msg='操作成功',$data=null):Response{
        return ApiResponse::apiSuccess($data, $msg);
    }

    protected function errorOptFail($msg='操作失败', $code=500){
        throw new ExceptionApi($msg, $code);
    }

    protected function errorRequest($msg='请求错误', $code=400){
        throw new ExceptionApi($msg, $code);
    }

}
