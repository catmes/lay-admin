<?php


namespace Catmes\LayAdmin\Controllers;

use App\Http\Response\VideoResponse;
use Catmes\LayAdmin\Components\LoginPage;
use Catmes\LayAdmin\Components\Menu;
use Catmes\LayAdmin\Components\Table;
use Catmes\LayAdmin\Components\TableData;
use Catmes\LayAdmin\Components\TableFieldSearch;
use Catmes\LayAdmin\Components\Template;
use App\Models\Video;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class IndexController extends BaseController
{
//    public function table(Request $request){}

    public function index(Request $request) :Response {
        $user = $request->user();
        $this->lay->setUsername($user->name);
        $html = $this->lay->render();
        return new Response($html);
    }

    public function layInit() :Response {
        return new Response($this->lay->getInitData());
    }

    public function layClear():Response{
        $json = '{"code": 1,"msg": "服务端清理缓存成功"}';
        return new Response($json);
    }

    public function test(Request $request):Response{
//        $template = Template::getInstance();
//        return new Response($template->view("form.php"));
        return new Response($request->all());
    }

    public function login():Response{
        $loginPage = new LoginPage();
        $loginPage->setLoginActionUrl("/admin/login")
            ->setFieldUsername('name');
        return new Response($loginPage);
    }
    public function register():Response{
        $loginPage = new LoginPage();
        $loginPage->setLoginActionUrl("/admin/register")
            ->setFieldUsername('name')
            ->setFieldEmail('email')
            ->setTitle('用户注册')
            ->setSubmitBtn('注册');
        return new Response($loginPage);
    }
}
